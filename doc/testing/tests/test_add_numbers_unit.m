function test_add_numbers_smoke()

    a = 1;
    b = 2;

    c = add_numbers(a, b);

    assert(c == 3);

end
